# USAGE
## html
This takes the Payload, encodes it in Base64 and embeds it into a HTML file.
If the file is opened, the file is decoded using Javascript and "downloaded.
Example:
```
trojantool html --output document.html --filename Document.docm Document.docm
```

Usage:
`trojantool html [flags]`

Flags:
```
      --filename string      Name of the File that gets Downloaded (default "Document.docm")
      --footer-name string   Name of the Company in the Footer (default "Made Up, Inc")
      --footer-url string    Name of the Company in the Footer (default "https://madeupinc.com")
  -h, --help                 help for html
      --output string        This is the Name of the Output file (default "Secure.html")
      --title string         This is the Title of the HTML page (default "Secure File Transfer")
```